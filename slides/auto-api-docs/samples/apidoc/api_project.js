define({
  "name": "example",
  "version": "0.1.0",
  "description": "apiDoc  example",
  "title": "apiDoc for apidoc collectinos",
  "url": "https://apid-doc.example.com/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-02-26T01:48:59.024Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
